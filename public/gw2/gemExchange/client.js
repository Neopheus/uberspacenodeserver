var socket;
var coins = 1000000;													//amount of coins for api request (100g)
var amountGems = 100;													//reference amount of gems
var priceHistory = [];												//stores prices
var lastChange = null;												//stores time of last price change
var alertAt = 0;															//alert if gold is less than x

var historyTime = 30;													//time in min
var maxMsDiff = historyTime*60*1000						//time in ms
var intervalId = null;
var updateTime = 1;														//update every x sec
var graphLimit = historyTime*(60/updateTime);	//last x prices

var graphContainer = null;

var graphLayout = {
	paper_bgcolor: "#000000",
	plot_bgcolor: "#000000",
	font: {
		color: "#FFFFFF"
	},
	yaxis: {
		title: "Gold per "+amountGems+" Gems",
		showline: true,
		linecolor: "#FFFFFF",
		linewidth: 2,
		showgrid: true,
		gridcolor: "#444444",
		gridwidth: 1,
	},
	xaxis: {
		title: "Time (hh:mm:ss)",
		tickformat: "%H:%M:%S",
		showline: true,
		linecolor: "#FFFFFF",
		linewidth: 2,
		showgrid: true,
		gridcolor: "#444444",
		gridwidth: 1,
	},
	margin: {
		l: 60,
		b: 60,
		r: 30,
		t: 5
	}
};

var graphOptions = {
	displayModeBar: false
};

$(document).ready(function(){
	graphContainer = $("#graph")[0];
	intervalId = setInterval(updateGemExchange, updateTime*1000);
});

$("#gems").change(function(){
	amountGems = $(this).val();
});

$("#gold, #silver, #copper").change(function(){
	updateTime = parseInt($("#gold").val()*1000+$("#silver").val()*10+$("#copper").val());
});

$("#update").change(function(){
	updateTime = $(this).val();
	clearInterval(intervalId);
	intervalId = setInterval(updateGemExchange, updateTime*1000);
});

function createTrace(data){
	var trace = {
		type: "scatter",
		mode: "lines",
		line: {
			width: 2,
			color: "#00FF00"
		},
		x: data.timestamps,
		y: data.prices
	};
	return trace;
}

function plotGraph(data, yTitle){
	graphLayout.yaxis.title = yTitle;
	Plotly.plot(graphContainer, [createTrace(data)], graphLayout, graphOptions);
}

function updateGraph(data, yTitle){
	Plotly.deleteTraces(graphContainer, 0);
	Plotly.addTraces(graphContainer, createTrace(data));
	graphLayout.yaxis.title = yTitle;
	Plotly.update(graphContainer, {}, graphLayout);
}

function updateGemExchange(){
	$.getJSON("https://api.guildwars2.com/v2/commerce/exchange/coins?quantity="+coins, function(exchange){
		$("#console").empty();
		consolelog("GW2 API Access:\n");
		consolelog("coins_per_gem: "+exchange.coins_per_gem);
		consolelog("quantity (for "+coins/10000+"g): "+exchange.quantity);
		consolelog("\n");

		var price = {
			gems: amountGems,
			raw: 0,
			gold: 0,
			silver: 0,
			copper: 0,
			tendency: null,
			timestamp: new Date(),
		}

		var gem1 = Math.round(coins/(exchange.quantity/100))/100;
		var gemX = gem1*amountGems;
		price.raw = gemX;
		var tmp = gemX;
		price.gold = (tmp-(tmp%10000))/10000;
		tmp = tmp%10000;
		price.silver = (tmp-(tmp)%100)/100;
		tmp = tmp%100;
		price.copper = (tmp-(tmp)%1)/1;
		if(priceHistory.length!=0){
			var curr = price.raw;
			var last = priceHistory[priceHistory.length-1].raw;
			price.tendency = (curr===last) ? "→" : ((curr<last) ? "↘" : "↗");
			lastChange = (price.tendency==="→") ? lastChange : price.timestamp;
		} else {
			lastChange = price.timestamp;
		}
		price.asString = price.gold+"g "+price.silver+"s "+price.copper+"c";

		consolelog("price (for "+amountGems+" gems): "+price.asString+" ("+price.timestamp+")");
		consolelog("tendency: "+price.tendency);
		consolelog("lastChange: "+lastChange);
		priceHistory.push(price);

		if(price.raw<alertAt && price.tendency!="→") notify("Price is below "+alertAt/10000+" Gold: "+amountGems+" Gems for "+price.asString);
		consolelog("<a target='_blank' href='http://www.gw2spidy.com/gem'>gw2spidy gem exchange</a>");

		var title = price.asString+" per "+amountGems+" Gems";
		if(graphContainer.childElementCount===1){
			updateGraph(historyToArrays(), title);
		} else {
			plotGraph(historyToArrays(), title);
		}
	});
}

function _historyToArrays(){
	var arrays = {
		timestamps: [],
		prices: []
	};
	var start = (priceHistory.length<graphLimit) ? 0 : priceHistory.length-graphLimit;
	for(var i=start; i<priceHistory.length; i++){
		if(priceHistory[i].gems===amountGems){
			arrays.timestamps.push(priceHistory[i].timestamp);
			arrays.prices.push(priceHistory[i].raw/10000);
		}
	}
	return arrays;
}

function historyToArrays(){
	var arrays = {
		timestamps: [],
		prices: []
	};
	var startIndex = priceHistory.length-1;
	var startTime = priceHistory[startIndex].timestamp;
	var msDiff = 0;
	for(var i=startIndex; i>=0&&msDiff<maxMsDiff; i--){
		if(priceHistory[i].gems===amountGems){
			arrays.timestamps.unshift(priceHistory[i].timestamp);
			arrays.prices.unshift(priceHistory[i].raw/10000);
		}
		msDiff = startTime - priceHistory[i].timestamp;
	}
	return arrays;
}

function notify(msg){
  if(!("Notification" in window)) {
    alert("This browser does not support desktop notification");
  } else if(Notification.permission === "granted") {
    var notification = new Notification(msg);
  }  else if(Notification.permission !== 'denied') {
    Notification.requestPermission(function (permission) {
      if (permission === "granted") {
        var notification = new Notification(msg);
      }
    });
  }
}

function consolelog(output){
	$("#console").show();
	$("#console").append(output+"\n");
}

function clearConsole(){
	$("#console").empty();
	$("#console").hide();
}
