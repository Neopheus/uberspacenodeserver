function createInfoButton(popuptext){
	var infoButton = $("<div id='infoButton'>?</div>");
	infoButton.css({
		"position": "fixed",
	  "bottom": "15px",
	  "right": "15px",
	  "width": "30px",
	  "height": "30px",
	  "background-color": "black",
	  "color": "white",
	  "font-size": "25px",
	  "font-weight": "bold",
	  "text-align": "center",
	  "border": "3px solid white",
	  "border-radius": "20px"
	});
	infoButton.click(function(){
		alert(popuptext);
	});
	$("body").prepend(infoButton);
}
