var language = navigator.language||navigator.userLanguage;
var lastSearchTerm = "";
var itemIds = [];
var itemLinks = {};
var useLinks = false;
var showTypes = [
	"Unlock",
	"Queue",
	"AccumulatingCurrency",
	"BankBag",
	"Claimable",
	"Boost",
	"Consumable"
];
var dontShow = [
	"Hub",
	"GuildHall",
	"GuildHallExpedition",
	"Decoration"
];
var opnUpg = null;
var oldScrollPos = 0;

$(document).ready(function(){
	//####################
	//### JQuery START ###
	//####################

	createInfoButton("Website created by Bernhard Maier (Neopheus.5680).");

	centerElement($("#searchbox"));
	language = (SUPPORTEDLANGUAGES.indexOf(language)!=-1) ? language: "en";
	$("#language").val(language);

	$("#search").on("change paste keyup", function(){
		var searchTerm = $(this).val().toLowerCase();
		$("#output").find(".upgrade").each(function(){
			var containsTerm = false;
			$(this).find(".upgradeName").each(function(){
				if($(this).html().toLowerCase().includes(searchTerm)){
					containsTerm = true;
					return false;
				}
			});
			if(containsTerm){
				$(this).show();
			} else {
				$(this).hide();
			}
		});
		lastSearchTerm = searchTerm;
		centerElement($("#output"));
	});

	$(window).scroll(function(){
		var newScrollPos = $(this).scrollTop();
		if(newScrollPos>oldScrollPos && $("#searchbox").is(":visible")){
			$("#searchbox").slideUp("fast");
		} else if(newScrollPos<oldScrollPos && !$("#searchbox").is(":visible")){
			$("#searchbox").slideDown("fast");
		}
		oldScrollPos = newScrollPos;
	});

	$("#clearSearch").click(function(){
		$("#search").val("");
		$("#search").trigger("change");
	});

	$("#language").change(function(){
		language = $(this).val();
		apiRequest(language);
	});

	$("#fullMsg").change(function(){
		apiRequest(language);
	});

	$("#itemCode").change(function(){
		apiRequest(language);
	});

	apiRequest(language);

//##################
//### JQuery END ###
//##################
});

function apiRequest(lang){
	$.getJSON("https://api.guildwars2.com/v2/guild/upgrades?ids=all&lang="+lang, function(upgrades){
		$("#output").empty();
		upgrades.sort(sortUpgrades);
		for(var i=0; i<upgrades.length; i++){
			if(showTypes.indexOf(upgrades[i].type)>-1){
				for(var j in upgrades[i].costs){
					var id = upgrades[i].costs[j].item_id;
					if(id!="undefined" && itemIds.indexOf(id)===-1){
						itemIds.push(id);
					}
				}
			}
		}
		if($("#itemCode").val()==="yes" && itemIds.length<=200){
			useLinks = true;
			$.getJSON("https://api.guildwars2.com/v2/items?ids="+idsToString(itemIds), function(items){
				for(var j in items){
					itemLinks[items[j].id] = items[j].chat_link;
				}
				for(var i=0; i<upgrades.length; i++){
					if(showTypes.indexOf(upgrades[i].type)>-1){
						$("#output").append(createUpgradeElement(upgrades[i]));
					}
				}
			});
		} else {
			useLinks = false;
			for(var i=0; i<upgrades.length; i++){
				if(showTypes.indexOf(upgrades[i].type)>-1){
					$("#output").append(createUpgradeElement(upgrades[i]));
				}
			}
		}
		centerElement($("#output"));
	})
	.fail(function(jqXHR, status, text){
		alert(status+": "+text+" ("+jqXHR.responseJSON.text+")");
		if(jqXHR.responseJSON.text==="API not active") createApiWarning();
	});
}

function createUpgradeElement(upg){
	var upgrade = $("<div class='upgrade'>");
	upgrade.hover(function(){
		$(this).addClass("upgShadow");
	}, function(){
		$(this).removeClass("upgShadow");
	});
	var upgradeHead = $("<div class='upgradeHead'>");
	upgradeHead.click(function(){
		if($(this).next().is(":visible")){
			collapsUpgrade($(this));
		} else {
			collapsUpgrade(opnUpg);
			expandUpgrade($(this));
		}
	});
	var arrowL = $("<span class='arrow arrowL'> ▼ </span>");
	var arrowR = $("<span class='arrow arrowR'> ▼ </span>");
	var upgradeName = $("<span class='upgradeName'>"+upg.name+" ("+upg.type+")</span>");
	var upgradeText = upgradeToText(upg);
	upgrade.append(
		upgradeHead.append(
			arrowL,
			upgradeName,
			arrowR
		),
		upgradeText
	);
	return upgrade;
}

function expandUpgrade(upg){
	if(upg){
		upg.next().slideDown("fast");
		upg.find(".arrow").html(" ▲ ");
		opnUpg = upg;
	}
}

function collapsUpgrade(upg){
	if(upg){
		upg.next().slideUp("fast");
		upg.find(".arrow").html(" ▼ ");
		opnUpg = null;
	}
}

function upgradeToText(upgrade){
	var output = $("<textarea class='upgradeText' cols='64' rows='20'></textarea>");
	var valueStr = "";
	var ath = 0;
	var fav = 0;
	var items = [];

	for(var i=0; i<upgrade.costs.length; i++){
		switch (upgrade.costs[i].type) {
			case "Item":
				items.push(upgrade.costs[i]);
				break;
			case "Collectible":
				fav = upgrade.costs[i].count;
				break;
			case "Currency":
				ath = upgrade.costs[i].count;
				break;
			default:
				//do nothing
		}
	}

	var fullMsg = $("#fullMsg").val()==="yes";
	if(fullMsg) {
		valueStr += "\n";
		valueStr += "⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯\n";
		valueStr += MESSAGES.GUILDHALL[language]+"(☐|☑)\n";
	}
	valueStr += "☐ "+upgrade.name+" ("+ath+"Ä|"+fav+"G)\n";
	for(var i=0; i<items.length; i++){
		var line = (i<items.length-1) ? " ┣" : " ┗";
		var name = (useLinks) ? itemLinks[items[i].item_id] : items[i].name;
		valueStr += line+"☐ 0/"+items[i].count+" "+name+"\n"
	}
	if(fullMsg){
		valueStr += "⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯⎯\n";
		valueStr += MESSAGES.THX[language];
	}

	output.val(valueStr);
	output.click(function(){
		if($("#clickncopy").val()==="yes"){
			this.select();
			if(document.execCommand("copy")) alert(MESSAGES.CLIPBOARD[language]);
		}
	});
	return output;
}

function idsToString(ids){
	var string = "";
	for(var i=0; i<ids.length; i++){
		string+=ids[i]+",";
	}
	return string;
}

function sortUpgrades(a, b){
	if(a.name < b.name) return -1;
	if(a.name > b.name) return 1;
	return 0;
}

function centerElement(elem){
	elem.css({
		"left": "50%",
		"margin-left": -elem.outerWidth()/2
	});
}

function createApiWarning(){
	var apiWarning = $("<div>");
	apiWarning.css({
		"position": "fixed",
		"bottom": "0px",
		"left": "0px",
		"right": "0px",
		"background-color": "#FF0000",
		"color": "#000000",
		"text-align": "center",
		"padding": "5px",
		"border-top": "2px solid black"
	});
	apiWarning.html(
		MESSAGES.APIDOWN[language]
	);
	$("body").append(apiWarning);
}


var SUPPORTEDLANGUAGES = [
	"de",
	"en",
	"es",
	"fr"
];

var MESSAGES = {
	GUILDHALL: {
		de: "Ausbau Gildenhalle: ",
		en: "Construction of guild hall: ",
		es: "Construcción de la nave gremial",
		fr: "Construction d'une salle de guilde"
	},
	THX: {
		de: "Großes Dankeschön an alle Spender!",
		en: "Many thanks to all donors!",
		es: "Muchas gracias a todos los donantes!",
		fr: "Un grand merci à tous les donateurs!"
	},
	CLIPBOARD: {
		de: "Text in Zwischenablage kopiert!",
		en: "Copy text to clipboard!",
		es: "Copiar texto al portapapeles!",
		fr: "Copiez du texte dans le presse-papiers!"
	},
	APIDOWN: {
		de: "API derzeit nicht verfügbar, bitte versuchen Sie es später noch einmal!",
		en: "API currently not available, please try again later!",
		es: "API actualmente no disponible, por favor inténtelo de nuevo más tarde!",
		fr: "API actuellement non disponible, veuillez réessayer plus tard!"
	},
};
