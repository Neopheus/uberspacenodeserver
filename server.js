var express = require("express");
var app = express();
var server = require("http").createServer(app);
var io = require("socket.io").listen(server);
var config = require("./config.json");

server.listen(config.port);
app.use(express.static(__dirname + "/public"));
app.get("/", function(req, res){
	res.sendFile(__dirname + "/public/index.html");
});


io.sockets.on("connection", function(socket){

	console.log("New User on Site.");
	socket.emit("connected");

});



//###################
//### END OF CODE ###
//###################

var serverRunning = "## Server is running on http://127.0.0.1:" + config.port + "/ ##";
var separator = "#".repeat(serverRunning.length);
console.log(separator);
console.log(serverRunning);
console.log(separator);
